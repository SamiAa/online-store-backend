const login = require('./login-routes');
const users = require('./users-routes');
const orders = require('./orders-routes');

module.exports = {
    login,
    users,
    orders
};