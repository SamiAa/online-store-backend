const express = require('express');
const router = express.Router();
const authenticate = require('../middleware/authenticate');
const usersControllers = require('../controllers/users-controllers');

router.post('/', usersControllers.createNewUser);

router.use(authenticate);

// router.get('/', async (req, res) => {
//     try {
//         const users = await User.find({}).lean();
//         res.status('200').json({ msg: users });
//     } catch (err) {
//         res.status('404').json({ msg: 'Could not access the user resources: ' + err });
//     }
// });

router.get('/:username', usersControllers.getAuthorizedUser);

router.put('/:username', usersControllers.updateUser);

router.get('/:username/orders', usersControllers.getUsersOrders);

module.exports = router;