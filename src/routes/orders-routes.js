const express = require('express');
const router = express.Router();
const authenticate = require('../middleware/authenticate');
const ordersControllers = require('../controllers/orders-controllers');

router.post('/', ordersControllers.createNewOrder);

// router.use(authenticate);

// router.get('/', ordersControllers.getAllOrders);

module.exports = router;