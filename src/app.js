require('dotenv').config();
const express = require('express');
const app = express();
const routes = require('./routes');
const cors = require('cors');
const port = process.env.PORT;
const connection = process.env.DB_CONNECTION_STRING;

// Mongoose connection
const mongoose = require('mongoose');
mongoose.connect(connection, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.on('open', function() {
    console.log("Connected to the database successfully!");
});

// Middleware
app.use(cors());
app.use(express.json());
// app.use((req, res, next) => {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
//     res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE");
//     next();
// });

// Routes
app.get('/', (req, res) => res.json({ msg: 'Welcome to online store backend!' }));
app.use('/login', routes.login);
app.use('/users', routes.users);
app.use('/orders', routes.orders);

// Listen port
app.listen(port, () => {
    console.log(`App is running at http://localhost:${port}`);
});