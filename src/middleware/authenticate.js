const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    // if (req.method === 'OPTIONS') return next();

    try {
        const header = req.headers.authorization;

        // If authorization header was found -> execute block
        if (header) {
            const token = header.split(' ')[1];
            const decodedToken = jwt.verify(token, process.env.TOKEN_SECRET_KEY);
            req.user = { username: decodedToken.username };
            next();
        } else {
            res.status(403).json({ msg: 'Authentication failed!' });
        }
    } catch (err) {
        next(err);
    }
}