const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/**
 * Log in to the system and send a json webtoken
 * @param {*} req 
 * @param {*} res 
 */
const login = async (req, res) => {
    try {
        const username = req.body.username;
        const password = req.body.password;
        let header = req.headers.authorization;
        let user;
        let token;

        // If the user has already logged in earlier, but is asking to
        // get logged in again with json webtoken
        // This happens when a user refreshes their browser
        if (typeof header !== 'undefined' &&
            typeof username === 'undefined' &&
            typeof password === 'undefined') {
                token = header.split(' ')[1];
                decodedToken = jwt.verify(token, process.env.TOKEN_SECRET_KEY);
                user = await User.findOne({ username: decodedToken.username });
                user.password = "";
                res.status('200').json({ msg: user });
        } else { // Log in the normal way with username and password
            user = await User.findOne({
                username: username
            }).lean();

            // Found a user with given username -> execute block
            if (user !== {} && user !== undefined && user !== null) {
                // Password is correct -> execute block
                if (bcrypt.compareSync(password, user.password)) {
                    user.password = "";
                    token = jwt.sign(
                        {
                            username: user.username
                        },
                        process.env.TOKEN_SECRET_KEY,
                        {
                            expiresIn: "30m"
                        }
                    );

                    // If json webtoken could not been created for some reason -> execute
                    if (!token) return res.status('404').json({ msg: 'Login process failed for some reason!' });

                    // If creadentials are correct and webtoken was created -> execute
                    res.status('200').json({ msg: user, token: token });
                } else {
                    res.status('403').json({ msg: 'Wrong username or password' });
                }
            } else {
                res.status('403').json({ msg: 'Wrong username or password' });
            }
        }
    } catch (err) {
        res.status('404').json({ msg: 'Could not access the user resources: ' + err });
    }
};

module.exports = {
    login
};