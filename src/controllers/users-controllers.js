const User = require('../models/User');
const Order = require('../models/Order');
const bcrypt = require('bcryptjs');

/**
 * Create a new user and send it into the database
 * @param {*} req 
 * @param {*} res 
 */
const createNewUser = async (req, res) => {
    try {
        const oldUser = await User.findOne({ username: req.body.username }).lean();

        // If the username is not taken yet -> execute block
        if (oldUser === {} || oldUser === undefined || oldUser === null ) {
            const salt = bcrypt.genSaltSync(12);
            const hashedPassword = bcrypt.hashSync(req.body.password, salt);
            let newUser = new User({
                username: req.body.username,
                password: hashedPassword,
                streetAddress: req.body.streetAddress,
                postNumber: req.body.postNumber,
                city: req.body.city
            });

            // Send the user into the database
            newUser = await newUser.save();
            // Send the enw users information back to the client
            res.status('200').json({ msg: newUser });
        } else {
            res.status('403').json({ msg: 'User with the given username already exists!' });
        }
    } catch (err) {
        res.status('404').json({ msg: 'Could not access the user resources: ' + err });
    }
};

/**
 * Get the information of the user that is logged in
 * When the user is asking their own data for some reason
 * @param {*} req 
 * @param {*} res 
 */
const getAuthorizedUser = async (req, res) => {
    try {
        const user = await User.findOne({ username: req.username });
        res.status('200').json({ msg: user });
    } catch (err) {
        res.status('404').json({ msg: err });
    }
};

/**
 * Update information of a user
 * Users can update only their own information
 * @param {*} req 
 * @param {*} res 
 */
const updateUser = async (req, res) => {
    try {
        const oldUsername = req.params.username;
        const newUsername = req.body.username;
        let canUpdate = false;

        // If the user did not change their username -> execute
        if (oldUsername === newUsername) {
            canUpdate = true;
        } else { // If the user did change their username -> execute block
            const oldUser = await User.findOne({ username: newUsername }).lean();

            // If the new username is not taken yet -> execute block
            if (oldUser === {} || oldUser === undefined || oldUser === null) {
                try {
                    const orders = await Order.updateMany({ user: oldUsername }, { user: newUsername });
                    canUpdate = true;
                } catch (err) {
                    res.status('404').json({ msg: err });
                }
            } else {
                res.status('403').json({ msg: 'User with the given username already exists!' });
            }
        }

        // If the username could be updated -> execute block
        if (canUpdate) {
            const salt = bcrypt.genSaltSync(12);
            let hashedPassword;

            // If the password was not changed -> execute block
            if (req.body.password === "thisisnothterealpassword") {
                const oldUser = await User.findOne({ username: oldUsername }).lean();
                hashedPassword = oldUser.password;
            } else { // If the password was changed -> execute
                hashedPassword = bcrypt.hashSync(req.body.password, salt);
            }

            let updatedUser = {
                username: newUsername,
                password: hashedPassword,
                streetAddress: req.body.streetAddress,
                postNumber: req.body.postNumber,
                city: req.body.city
            };
    
            // Update the users information into the database
            updatedUser = await User.findOneAndUpdate({ username: oldUsername }, updatedUser, { useFindAndModify: false });
            
            // If the update process succeeded -> execute
            if (updatedUser !== {} && updatedUser !== undefined && updatedUser !== null)
                res.status('200').json({ msg: updatedUser });
            else { // If the update process failed -> execute
                res.status('404').json({ msg: 'Could not find user with the given username!' });
            }
        }
    } catch (err) {
        res.status('404').json({ msg: 'Could not access the user resources: ' + err });
    }
};

/**
 * Get orders of a specific user
 * @param {*} req 
 * @param {*} res 
 */
const getUsersOrders = async (req, res) => {
    try {
        const username = req.params.username;
        const orders = await Order.find({ user: username }).sort('-date').lean();
        res.status('200').json({ msg: orders });
    } catch (err) {
        res.status('404').json({ msg: err });
    }
};

module.exports = {
    createNewUser,
    getAuthorizedUser,
    updateUser,
    getUsersOrders
};