const Order = require('../models/Order');

/**
 * Create a new order and send it into the database
 * @param {*} req 
 * @param {*} res 
 */
const createNewOrder = async (req, res) => {
    try {
        let newOrder = new Order({
            user: req.body.user,
            deliveryAddress: req.body.deliveryAddress,
            products: req.body.products,
            cost: req.body.cost,
            paymentMethod: req.body.paymentMethod,
            flag: req.body.flag,
            date: req.body.date
        });

        // Send the new user into the database
        newOrder = await newOrder.save();
        res.status('200').json({ msg: newOrder });
    } catch (err) {
        res.status('404').json({ msg: 'Could not access the order resources: ' + err });
    }
};

/**
 * Get all orders from the database
 * @param {*} req 
 * @param {*} res 
 */
const getAllOrders = async (req, res) => {
    try {
        const orders = await Order.find({}).lean();
        res.status('200').json({ msg: orders });
    } catch (err) {
        res.status('404').json({ msg: err });
    }
};

module.exports = {
    createNewOrder,
    getAllOrders
}