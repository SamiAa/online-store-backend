const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: { type: String, minlength: 1, maxlength: 100, required: true },
    password: { type: String, minlength: 1, maxlength:100, required: true },
    streetAddress: { type: String, minlength: 1, maxlength: 100, required: true },
    postNumber: { type: String, minlength: 5, maxlength: 5, required: true },
    city: { type: String, minlength: 1, maxlength: 100, required: true }
});

const User = mongoose.model('User', userSchema);

module.exports = User;