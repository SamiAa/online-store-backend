const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    user: { type: String },
    deliveryAddress: { type: String, required: true },
    products: { type: Array, required: true },
    cost: { type: Number, required: true },
    paymentMethod: { type: String, minlength: 1, maxlength: 20, required: true },
    flag: { type: String, minlength: 1, maxlength: 20, required: true },
    date: { type: String, required: true }
});

const Order = mongoose.model('Order', orderSchema);

module.exports = Order;